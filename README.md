# Windows Setup

```powershell
iex "& { $(irm https://gitlab.com/fivefingergames/windows-setup/-/raw/master/install.ps1) } -For developer"
```

1. Debloat => `iwr -useb https://git.io/debloat | iex`
2. Uninstall all other software you don't need
3. Install packages manager (scoops & choco)
    3.1 Install `scoop` => https://scoop.sh
    3.2 Install `choco` => https://chocolatey.org/install
4. Install scoops buckets
    4.1 We need git to install buckets =>  `scoop install git`
    4.2 `scoop bucket add main`
    4.3 `scoop bucket add extras`
    4.4 `scoop bucket add versions`
    4.5 `scoop bucket add nerd-fonts`
5. Install required scoops packages
6. Install required choco packages
7. Install WSL => `wsl --install`
8. Change computer name
9. Reboot => `Restart-Computer`
10. Finish WSL installation
11. Ensure `nvm` has correct `node` versions installed
