param (
    [string]$version = "11"
)

$env:WIN_VERS = $version
vagrant destroy -f
vagrant up
