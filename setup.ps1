param (
    [parameter(Mandatory = $true)]
    [string]
    $For
)

$config = Get-Content -Raw "config.json" | ConvertFrom-Json

function Install-Choco {
    try {
        Set-ExecutionPolicy Bypass -Scope Process -Force;
        [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072;
        Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1')) | Out-Null;
    }
    catch { }
}

function Install-Scoop {
    try {
        Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
        Invoke-Expression "& {$(Invoke-RestMethod get.scoop.sh)} -RunAsAdmin"
        scoop install git
    }
    catch { }
}

function Install-Packages {
    Install-Scoop
    Install-Choco

    foreach ($bucket in $config.packages.scoop.buckets) {
        scoop bucket add $bucket
    }

    $postInstallScripts = @();
    $scoopPackages = $config.packages.scoop.base;
    $chocoPackages = $config.packages.choco.base;

    $scoopPackages += $config.packages.scoop.$For;
    $chocoPackages += $config.packages.choco.$For;

    foreach ($scoopPackage in $scoopPackages) {
        scoop install $scoopPackage.name
        $postInstallScripts += $scoopPackage.post;
    }

    foreach ($chocoPackage in $chocoPackages) {
        choco install $chocoPackage.name -y
        $postInstallScripts += $chocoPackage.post;
    }

    $env:Path =
    [System.Environment]::GetEnvironmentVariable("Path", "Machine") +
    ";" +
    [System.Environment]::GetEnvironmentVariable("Path", "User");

    foreach ($script in $postInstallScripts) { Invoke-Expression $script }
}

function Set-Settings {
    foreach ($setting in $config.settings) {
        Write-Output $setting.msg
        Set-ItemProperty -Path $setting.path -Name $setting.name -Value $setting.value
    }

    rundll32.exe user32.dll, UpdatePerUserSystemParameters
}


# Invoke-WebRequest -useb https://git.io/debloat | Invoke-Expression
Install-Packages
Set-Settings
if ($For -eq "developer") {
    $packages = @();
    foreach ($chocoPackage in $config.packages.choco.$For) { $packages += $chocoPackage.name; }

    if ($packages.Contains("wsl") -or $packages.Contains("wsl2")) {
        $continueScriptPath = "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Startup\continue.bat"
        Set-Content $continueScriptPath "start powershell $PSScriptRoot\reboot.ps1; del %~dpnx0"

        Write-Output "Restarting in 5 seconds for the WSL installation to take effect..."
        Start-Sleep -Seconds 5
        Restart-Computer

        # The script will continue after the reboot, in the reboot.ps1 script.
    }
}
