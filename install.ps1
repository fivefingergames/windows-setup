param (
    [parameter(Mandatory = $true)]
    [string]
    $For
)

$cwd = $pwd.path

$url = "https://gitlab.com/fivefingergames/windows-setup/-/archive/master/windows-setup-master.zip"
$downloadPath = "$cwd/" + $(Split-Path -Path $Url -Leaf)
$archiveName = $(Split-Path -Path $Url -LeafBase)

Invoke-WebRequest -Uri $url -OutFile $downloadPath
Expand-Archive -Path $downloadPath -DestinationPath $cwd -Force
Remove-Item -Path $downloadPath
Invoke-Expression "./$archiveName/setup.ps1 -For $For"
